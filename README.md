# The VOD Machine Version 1.0

## Installation

Requires node.js and was built using v6.10.2.

Also recommended to have gulp and karma installed globally. `npm install gulp karma -g`

Open terminal and navigate to the root of the repo, then download the required packages.

```npm install```

## Usage

After you have downloaded the packages, you can now build the css

```gulp build```

and then you can start the app

```npm start```

Now using your browser, navigate to http://localhost:3000.

Unit tests can be run by typing

```gulp test```

After the tests have run for the first time, you can find a 'Coverage' folder in the root directory. This contains a HTML file that reports on unit test coverage.

## Requirments

I think I have made a good attempt at everything in the Mandatory Features list.

From the optional features list, I have attempted
- Persistent storage of watched items
- Unit tests

## Known bugs

There is no progress bar on my player.

I did not achieve the coverage of unit testing I would have liked.

Overall there parts of the app I wish I could have spent more time with, and there are other optional features that I wish I could have delivered, but in the end I ran out of time to do much more.

Thank you for giving me a shot at the test.

Best regards
Michael
