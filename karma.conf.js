/*jslint node: true */
'use strict';

module.exports = function(config) {
  config.set({
    basePath: './',
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    colors: true,
    files: [
      // lib files
      'node_modules/jquery/dist/jquery.js',
      'node_modules/angular/angular.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'node_modules/@uirouter/angularjs/release/angular-ui-router.js',
      'node_modules/angular-resource/angular-resource.js',
      'node_modules/jasmine-jquery/lib/jasmine-jquery.js',

      // project files
      'src/client/app/app.module.js',
      'src/client/app/**/*.js',
      'src/client/app/**/*.html',

      // fixtures
      {pattern: 'src/client/**/*.spec.json', watched: true, served: true, included: false}
    ],
    reporters: ['spec', 'coverage'],
    coverageReporter: {
      type: 'html',
      dir: 'coverage',
      includeAllSources: true
    },
    preprocessors: {
      'src/client/app/**/!(*spec).js': ['coverage'],
      'src/client/app/**/*.html': ['ng-html2js']
    },
    ngHtml2JsPreprocessor: {
      stripPrefix: 'src/client',
      moduleName: 'templates'
    },
    singleRun: true
  });
};
