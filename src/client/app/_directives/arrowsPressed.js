(function () {
  'use strict';

  var app = angular.module('vodMachine');

  app.directive('arrowsPressed', function () {
    return function (scope) {
      scope.keydown = function (e) {
        var target;

        if (e.keyCode === 37) {
          e.preventDefault();
          target = angular.element(e.target)[0].previousElementSibling;
          if (target) {
            target.focus();
          }
        }

        if (e.keyCode === 39) {
          e.preventDefault();
          target = angular.element(e.target)[0].nextElementSibling;
          if (target) {
            target.focus();
          }
        }

        if (e.keyCode === 13) {
          scope.playVideo();
        }
      };
    };
  });
}());
