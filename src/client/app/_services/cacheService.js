(function () {
  'use strict';

  var app = angular.module('vodMachine');

  app.service('cacheService', ['$window', function ($window) {
    var getKey,
      setKey;

    getKey = function (key) {
      var result = $window.localStorage.getItem(key),
        expiry = $window.localStorage.getItem(key + '_exp');

      if (!result || (expiry && expiry < Date.now())) {
        return undefined;
      }

      return JSON.parse(result);
    };

    setKey = function (key, value, skipExp) {
      var expires = Date.now() + (1000 * 60 * 15);

      $window.localStorage.setItem(key, JSON.stringify(value));
      if (!skipExp) {
        $window.localStorage.setItem(key + '_exp', expires);
      }
    };

    return {
      getKey: getKey,
      setKey: setKey
    };
  }]);
}());
