/* jslint node: true */
/* global inject, expect, jasmine */

(function () {
  'use strict';

  describe('cacheService - for writing to localStorage', function () {
    var cacheService,
      windowMock,
      result,
      key,
      value;

    beforeEach(function () {
      windowMock = {
        localStorage: {
          value: {},
          getItem: jasmine.createSpy().and.callFake(function () {
            key = arguments[0];
            return windowMock.localStorage.value[key];
          }),
          setItem: jasmine.createSpy().and.callFake(function () {
            key = arguments[0];
            value = arguments[1];

            windowMock.localStorage.value[key] = value;
          })
        }
      };

      module('vodMachine', function ($provide) {
        $provide.value('$window', windowMock);
      });
    });

    beforeEach(inject(function (_cacheService_) {
      cacheService = _cacheService_;
    }));

    it('should ensure cacheService exist', function() {
      expect(cacheService).toBeDefined();
    });

    it('should check for a key from localStorage, and return its value', function () {
      result = cacheService.getKey('vodMachineCookie');

      expect(windowMock.localStorage.getItem).toHaveBeenCalledWith('vodMachineCookie');
      expect(result).not.toBeDefined();

      cacheService.setKey('vodMachineCookie', { name: 'homer' });
      result = cacheService.getKey('vodMachineCookie');

      expect(windowMock.localStorage.getItem).toHaveBeenCalledWith('vodMachineCookie');
      expect(result).toEqual({ name: 'homer' });
    });

    it('should set localStorage entry for the key value pair provided', function () {
      result = cacheService.setKey('vodMachineCookie', { name: 'homer' });

      expect(windowMock.localStorage.setItem.calls.count()).toBe(2);
      expect(windowMock.localStorage.value.vodMachineCookie).toEqual(JSON.stringify({ name: 'homer' }));
      expect(windowMock.localStorage.value.vodMachineCookie_exp).toBeDefined();
    });

  });
}());
