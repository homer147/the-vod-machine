(function () {
  'use strict';

  var app = angular.module('vodMachine');

  app.service('scopeShareService', function () {
    var videos,
      videoSlug,
      getVideos,
      putVideos,
      notifyVideoObservers,
      registerVideoObservers,
      sendingObserverCallbacks = [],
      getVideoSlug,
      putVideoSlug,
      registerVideoSlugObservers,
      notifyVideoSlugObservers,
      VsObserverCallbacks = [],
      getWatchedVideos,
      watchedVideos,
      putWatchedVideos,
      registerWatchVideoObservers,
      WvObserverCallbacks = [],
      notifyWatchVideoObservers;

    // video entries
    getVideos = function () {
      return videos;
    };

    putVideos = function (newValue) {
      videos = newValue;
      notifyVideoObservers();
      return videos;
    };

    registerVideoObservers = function (callback) {
      sendingObserverCallbacks.push(callback);
    };

    notifyVideoObservers = function () {
      angular.forEach(sendingObserverCallbacks, function (callback) {
        callback();
      });
    };

    // active video slug
    getVideoSlug = function () {
      return videoSlug;
    };

    putVideoSlug = function (newValue) {
      videoSlug = newValue;
      notifyVideoSlugObservers();
      return videoSlug;
    };

    registerVideoSlugObservers = function (callback) {
      VsObserverCallbacks.push(callback);
    };

    notifyVideoSlugObservers = function () {
      angular.forEach(VsObserverCallbacks, function (callback) {
        callback();
      });
    };

    // watched videos
    getWatchedVideos = function () {
      return watchedVideos;
    };

    putWatchedVideos = function (newValue) {
      watchedVideos = newValue;
      notifyWatchVideoObservers();
      return watchedVideos;
    };

    registerWatchVideoObservers = function (callback) {
      WvObserverCallbacks.push(callback);
    };

    notifyWatchVideoObservers = function () {
      angular.forEach(WvObserverCallbacks, function (callback) {
        callback();
      });
    };

    return {
      // videos
      getVideos: getVideos,
      putVideos: putVideos,
      registerVideoObservers: registerVideoObservers,

      // slug
      getVideoSlug: getVideoSlug,
      putVideoSlug: putVideoSlug,
      registerVideoSlugObservers: registerVideoSlugObservers,

      // watched videos
      getWatchedVideos: getWatchedVideos,
      putWatchedVideos: putWatchedVideos,
      registerWatchVideoObservers: registerWatchVideoObservers
    };
  });

}());
