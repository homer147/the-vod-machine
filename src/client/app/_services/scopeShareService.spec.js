/* jslint node: true */
/* global inject, expect, jasmine */

(function () {
  'use strict';

  describe('scopeShareService - service for sharing scope', function () {
    var scopeShareService,
      result;

    beforeEach(function () {
      module('vodMachine');
    });

    beforeEach(inject(function (_scopeShareService_) {
      scopeShareService = _scopeShareService_;
    }));

    it('should ensure cacheService exist', function() {
      expect(scopeShareService).toBeDefined();
    });

    it('should put and then get a value from scopeShareService', function () {

      // videos
      result = scopeShareService.putVideos({video1: 'video1'});
      expect(result).toEqual({video1: 'video1'});
      result = scopeShareService.getVideos();
      expect(result).toEqual({video1: 'video1'});

      // video slugs
      result = scopeShareService.putVideoSlug('video1-video');
      expect(result).toEqual('video1-video');
      result = scopeShareService.getVideoSlug();
      expect(result).toEqual('video1-video');

      // watcged videos
      result = scopeShareService.putWatchedVideos({video1: 'video1'});
      expect(result).toEqual({video1: 'video1'});
      result = scopeShareService.getWatchedVideos();
      expect(result).toEqual({video1: 'video1'});
    });
  });
}());
