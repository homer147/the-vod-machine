(function () {
  'use strict';

  var app = angular.module('vodMachine');

  app.service('videoService', ['$resource', function ($resource) {
    return $resource('https://demo2697834.mockable.io/movies');
  }]);
}());
