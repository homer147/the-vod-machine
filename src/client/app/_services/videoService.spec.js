/* jslint node: true */
/* global inject, expect, jasmine, getJSONFixture */

(function () {
  'use strict';

  describe('videoService - calls for videos', function () {
    var videoService,
      $httpBackend,
      mockResponse,
      result;

    beforeEach(function () {
      module('vodMachine');
    });

    beforeEach(inject(function (_videoService_, $injector) {
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app/home';
      mockResponse = getJSONFixture('home.spec.json');
      videoService = _videoService_;
      $httpBackend = $injector.get('$httpBackend');

      $httpBackend.when('GET', 'https://demo2697834.mockable.io/movies')
        .respond(200, mockResponse);
    }));

    it('should ensure videoService exists', function () {
      expect(videoService).toBeDefined();
    });

    it('should have made the http request to get the videos', function () {
      $httpBackend.when('GET', '/app/home/home.html').respond(200);
      result = videoService.get();
      expect($httpBackend.flush).not.toThrow();
      //expect(result).toEqual(mockResponse);
    });
  });
}());
