(function () {
  'use strict';

  var app = angular.module('vodMachine', [
    'ui.router',
    'ngResource'
  ]);

  app.config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
    function ($urlRouterProvider, $stateProvider, $locationProvider) {
      $urlRouterProvider.otherwise('/');

      $stateProvider
        .state('home', {
          url: '/',
          templateUrl: '/app/home/home.html',
          controller: 'homeCtrl'
        });

      $locationProvider.html5Mode(true);
    }]);
}());
