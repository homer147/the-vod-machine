(function () {
  'use strict';

  var app = angular.module('vodMachine');

  app.controller('homeCtrl', ['$scope', 'videoService', 'cacheService', 'scopeShareService',
    function ($scope, videoService, cacheService, scopeShareService) {
      var vodMachineCookie = cacheService.getKey('vodMachineCookie'),
        videoWatchedCookie = cacheService.getKey('watchedVideos'),
        watchedVideids,
        i,
        j;

      $scope.showVideo = false;
      $scope.watchedVideos = [];

      function getVideos() {
        videoService.get(function (data) {
          $scope.videos = scopeShareService.putVideos(data);
          cacheService.setKey('vodMachineCookie', data);
          return data;
        });
      }

      function updateSendingVar() {
        $scope.videos = scopeShareService.getVideos();
      }

      function updateVsVar() {
        $scope.slug = scopeShareService.getVideoSlug();
        if ($scope.slug === undefined) {
          $scope.showVideo = false;
        }
      }

      function updateWvVar() {
        watchedVideids = scopeShareService.getWatchedVideos();
        updateWatched();
      }

      function updateWatched() {
        for (i = 0; i < watchedVideids.length; i++) {
          for (j = 0; j < $scope.videos.entries.length; j++) {
            if (watchedVideids[i] === $scope.videos.entries[j].id) {
              if ($scope.watchedVideos.indexOf($scope.videos.entries[j]) === -1) {
                $scope.watchedVideos.unshift($scope.videos.entries[j]);
              }
            }
          }
        }
      }

      if (vodMachineCookie) {
        $scope.videos = scopeShareService.putVideos(vodMachineCookie);
      } else {
        getVideos();
      }

      if (videoWatchedCookie) {
        watchedVideids = videoWatchedCookie;
        updateWatched();
      }

      $scope.playVideo = function (slug) {
        $scope.slug = scopeShareService.putVideoSlug(slug);
        $scope.showVideo = true;
      };

      scopeShareService.registerVideoObservers(updateSendingVar);
      scopeShareService.registerVideoSlugObservers(updateVsVar);
      scopeShareService.registerWatchVideoObservers(updateWvVar);
    }]);
}());
