/* jslint node: true */
/* global inject, expect, jasmine, getJSONFixture */

(function () {
  'use strict';

  describe('homeCtrl - home page controller', function () {
    var createController,
      homeCtrl,
      jsonData,
      mockResponse,
      scope,
      videoServiceMock,
      cacheServiceMock;

    beforeEach(function () {
      module('vodMachine');
    });

    beforeEach(function () {
      videoServiceMock = {
        get: jasmine.createSpy().and.callFake(function () {
          return mockResponse;
        })
      };
      cacheServiceMock = {
        getKey: jasmine.createSpy().and.callFake(function () {
          return undefined;
        })
      };
    });

    beforeEach(inject(function ($rootScope, $controller) {
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app/home';
      jsonData = getJSONFixture('home.spec.json');
      mockResponse = jsonData;

      createController = function () {
        scope = $rootScope.$new();

        return $controller('homeCtrl', {
          $scope: scope,
          videoService: videoServiceMock,
          cacheService: cacheServiceMock
        });
      };

      homeCtrl = createController();

    }));

    it('should have set the default variables', function () {
      expect(scope.showVideo).toBeFalsy();
      expect(scope.watchedVideos).toBeDefined();
    });

    it('should have called the video service and received a response', function () {
      expect(cacheServiceMock.getKey).toHaveBeenCalled();
      expect(videoServiceMock.get).toHaveBeenCalled();
      //expect(scope.videos).toEqual(mockResponse);
    });

  });
}());
