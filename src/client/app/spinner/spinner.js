(function () {
  'use strict';

  var app = angular.module('vodMachine');

  app.directive('spinner', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/spinner/spinner.html',
    };
  });
}());
