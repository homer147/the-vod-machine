(function () {
  'use strict';

  var app = angular.module('vodMachine');

  app.controller('videoPageCtrl', ['$scope', 'scopeShareService',
    function ($scope, scopeShareService) {
      var i;

      $scope.videos = scopeShareService.getVideos();
      $scope.video = {};
      $scope.slug = scopeShareService.getVideoSlug();

      function updateSendingVar() {
        $scope.slug = scopeShareService.getVideoSlug();
        checkSlug();
      }

      function checkSlug() {
        if ($scope.slug) {
          for (i = 0; i < $scope.videos.entries.length; i++) {

            if ($scope.videos.entries[i].id === $scope.slug) {
              $scope.video = $scope.videos.entries[i];
              $scope.videoUrl = $scope.video.contents[0].url;
            }
          }
        }
      }

      checkSlug();

      scopeShareService.registerVideoSlugObservers(updateSendingVar);
    }]);

  app.directive('videoPlayer', ['scopeShareService', 'cacheService',
    function (scopeShareService, cacheService) {
      return {
        restrict: 'E',
        replace: true,
        templateUrl: '/app/videoPage/videoPage.html',
        controller: 'videoPageCtrl',
        link: function (scope) {
          var player = document.getElementById('video'),
            watchedCache = cacheService.getKey('watchedVideos'),
            firstPlay = true,
            i;

          if (!watchedCache) {
            watchedCache = [];
          }

          scope.playing = false;

          function changeButtonType() {
            scope.playing = !scope.playing;
          }

          function checkWatchedCache() {
            var inCache = false;

            for (i = 0; i < watchedCache.length; i++) {
              if (watchedCache[i] === scope.video.id) {
                inCache = true;
                return;
              }
            }

            if (!inCache) {
              watchedCache.push(scope.video.id);
              cacheService.setKey('watchedVideos', watchedCache, true);
              scopeShareService.putWatchedVideos(watchedCache);
            }
          }

          scope.closeVideo = function () {
            player.pause();
            scope.slug = scopeShareService.putVideoSlug(undefined);
            scope.videoUrl = undefined;
            scope.playing = false;
            firstPlay = true;
          };

          scope.pausePlay = function () {
            if (firstPlay) {
              checkWatchedCache();
              firstPlay = false;
            }

            if (player.paused || player.ended) {
              changeButtonType();
              player.play();
            } else {
              changeButtonType();
              player.pause();
            }
          };

          scope.fullScreen = function () {
            if (player.requestFullscreen) {
              if (document.fullScreenElement) {
                document.cancelFullScreen();
              } else {
                player.requestFullscreen();
              }
            } else if (player.msRequestFullscreen) {
              if (document.msFullscreenElement) {
                document.msExitFullscreen();
              } else {
                player.msRequestFullscreen();
              }
            } else if (player.mozRequestFullScreen) {
              if (document.mozFullScreenElement) {
                document.mozCancelFullScreen();
              } else {
                player.mozRequestFullScreen();
              }
            } else if (player.webkitRequestFullscreen) {
              if (document.webkitFullscreenElement) {
                document.webkitCancelFullScreen();
              } else {
                player.webkitRequestFullscreen();
              }
            }
          };
        }
      };
    }]);
}());
